# dr_quine

A quine is basically a program that write it's own source code.
For this project we had to do 3 quine that are graduatly more difficult.

## Description

- Colleen:
    This one is simple enough to start understanding how quine are made.
    And so with some research I learned that a quine in C works with a format string and printf
    ```C
    // General algorythm of a quine in C
    char f[] = "__CODE_PRINTF_FORMATED__";
    int main (int ac, char **av) {
        printf(f, __SOME_VARS_, f, _SOME_VARS_);
    }
    ```
    And so, we can see that the with the first f we write the file. And with the second one we manage to write it's own format again.
    After this the trick is using diff until there are no diffs.
- Grace:
    This one is that you have to create a new file and write the code inside instead of outputting it.
    Plus you'll need to define you main.
- Sully:
    The last one like Grace but you input a number to the program and it will create n file and create n executables.
    But not 0 to stop it;
