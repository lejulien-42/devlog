import { Component, Input } from '@angular/core';
import { marked } from 'marked';
import { HttpClient } from '@angular/common/http';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-markdown-displayer',
  templateUrl: './markdown-displayer.component.html',
  styleUrls: ['./markdown-displayer.component.css']
})
export class MarkdownDisplayerComponent {
  @Input() path = '';
  convertData: any;

  constructor(private http: HttpClient) {}

  ngOnInit() {
    this.getFileData().subscribe(response => {
      var md = marked.setOptions({});
      this.convertData = md.parse(response);
    });
  }

  getFileData() {
    return this.http.get(this.path, {
          responseType: 'text'
        });
  }
}
