import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MarkdownDisplayerComponent } from './markdown-displayer.component';

describe('MarkdownDisplayerComponent', () => {
  let component: MarkdownDisplayerComponent;
  let fixture: ComponentFixture<MarkdownDisplayerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MarkdownDisplayerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MarkdownDisplayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
