import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { MarkdownDisplayerComponent } from './components/markdown-displayer/markdown-displayer.component';

@NgModule({
  declarations: [
    AppComponent,
    MarkdownDisplayerComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
